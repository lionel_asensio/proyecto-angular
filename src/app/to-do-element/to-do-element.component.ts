import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { Tarea } from '../models/tarea.models';

@Component({
  selector: 'app-to-do-element',
  templateUrl: './to-do-element.component.html',
  styleUrls: ['./to-do-element.component.scss']
})
export class ToDoElementComponent implements OnInit {

  @Input() tarea: Tarea;
  @HostBinding('attr.class') cssClass = 'col-md-4';

  constructor() { }

  ngOnInit(): void {
  }

}
