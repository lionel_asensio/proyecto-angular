import { Component, OnInit } from '@angular/core';
import { Tarea } from '../models/tarea.models';

@Component({
  selector: 'app-to-do-list',
  templateUrl: './to-do-list.component.html',
  styleUrls: ['./to-do-list.component.scss']
})
export class ToDoListComponent implements OnInit {

  tareas: Tarea[];

  constructor() {
    this.tareas = [];
  }

  ngOnInit(): void {
  }

  agregar(nombre: string): boolean {
    this.tareas.push(new Tarea(nombre));
    return false;
  }

}
